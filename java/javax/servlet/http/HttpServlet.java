/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package javax.servlet.http;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.ResourceBundle;

import javax.servlet.DispatcherType;
import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


/**
 * 用于创建HTTP servlet的抽象类。
 * <code>HttpServlet</code>的子类至少要重写下面任意一个方法:
 * <ul>
 * <li> <code>doGet</code>, GET请求
 * <li> <code>doPost</code>, POST请求
 * <li> <code>doPut</code>, PUT请求
 * <li> <code>doDelete</code>, DELETE请求
 * <li> <code>init</code> 和 <code>destroy</code>,
 * servlet生命周期中的资源信息，你可以调用<li> <code>getServletInfo</code>获得
 * </ul>
 *
 * <p>不要覆盖<code>service</code>方法。
 * <code>service</code>方法根据HTTP请求类型分发到指定的方法(上面列举的类型和对应的方法)。
 *
 * <p>同样, 不要覆盖<code>doOptions</code>或者<code>doTrace</code>方法.
 *
 * <p>Servlet通常运行在多线程环境中,所以必须谨慎地处理并发中遇到的共享资源和锁的问题。
 * 共享资源包括内存数据(实例，类变量)和外部资源(文件、数据库连接、网络连接)等资源。
 * 你可以往访问<a href="http://java.sun.com/Series/Tutorial/java/threads/multithreaded.html">
 * Java 多线程编程教程</a> 获取更多java处理多线程问题的相关资料.
 */
public abstract class HttpServlet extends GenericServlet {

    private static final long serialVersionUID = 1L;

    private static final String METHOD_DELETE = "DELETE";
    private static final String METHOD_HEAD = "HEAD";
    private static final String METHOD_GET = "GET";
    private static final String METHOD_OPTIONS = "OPTIONS";
    private static final String METHOD_POST = "POST";
    private static final String METHOD_PUT = "PUT";
    private static final String METHOD_TRACE = "TRACE";

    private static final String HEADER_IFMODSINCE = "If-Modified-Since";
    private static final String HEADER_LASTMOD = "Last-Modified";

    private static final String LSTRING_FILE =
        "javax.servlet.http.LocalStrings";
    private static final ResourceBundle lStrings =
        ResourceBundle.getBundle(LSTRING_FILE);


    /**
     * 啥都不做，因为这是个抽象类.
     */
    public HttpServlet() {
        // NOOP
    }


    /**
     * <code>service</code>方法调用此方法处理GET类型的http请求。
     *
     * <p>HEAD请求是一个特殊的GET请求，他在响应中不返回主体，只返回响应头。
     * 所以，重写这个方法就相当于重写了处理HEAD请求的方法。
     *
     * <p>当重写这个方法的时候, 会先读取请求数据, 然后写响应头， 最后编写响应主体。
     * 切记，最好定义好content type和encoding。
     * 当获取<code>PrintWriter</code>来编写响应主体的时候，记住先设置content type.
     *
     * <p>servlet容器返回response消息体之前，必须编写响应头, 因为HTTP头部信息必须先于response消息体.
     *
     * <p>尽量用{@link javax.servlet.ServletResponse#setContentLength}方法指定Content-Length,
     * 因为这样允许servlet会使用一个持久连接来返回数据，提高了性能。
     * 如果整个响应刚好充满一个缓冲区，则会自动设置内容长度。
     *
     * <p>当使用HTTP 1.1 分块编码 这意味着响应具有Transfer-Encoding报头)，不要设置Content-Length报头。
     *
     * <p>GET方法应该是安全的, 这意味着它不应该有状态的改变.
     * 比如大部分数据查询请求，就不会有状态的改变，因为他不会修改数据库里面的值。
     * 如果请求确实需要改变已经存储好的数据,那么它应该使用其他请求方法.
     *
     * <p>GET请求应该是幂等的，这意味着无论访问多少次，它的请求结果都不会因为上一次的请求而改变。
     *
     * <p>如果请求格式错误, <code>doGet</code>方法返回"Bad Request"信息.
     *
     * @param req   {@link HttpServletRequest}对象，包含请求信息
     *
     * @param resp  {@link HttpServletResponse}对象， 返回信息
     *
     * @exception IOException   输入输入检测到异常的时候抛出的异常
     *
     * @exception ServletException  如果这个请求servlet无法处理
     *
     * @see javax.servlet.ServletResponse#setContentType
     */
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {
        String protocol = req.getProtocol();
        String msg = lStrings.getString("http.method_get_not_supported");
        if (protocol.endsWith("1.1")) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, msg);
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
        }
    }


    /**
     * 返回 <code>HttpServletRequest</code>请求的返回体上次被修改的时间,
     * 从 January 1, 1970 GMT开始计算的毫秒数。
     * 如果时间未知，返回一个负数(默认-1).
     *
     * <p>这个方法的好处是可以减少服务器和网络资源的负载，让浏览器和代理缓存工作更高效。
     *
     * qzr-add: 怎么提高呢，要知道我们的浏览器可以设置不强制每次刷新的，这个时候，页面内容更改了怎么办呢？
     *          答案就是在这个方法。这个方法会返回一个最后修改时间，下次浏览器带着它来请求，然后再取一个新的来比较，
     *          如果新的更新，那就返回新的内容。否则直接返回304，用本地的。不过默认的方法啥都没做，你懂的。
     *
     * @param req   the <code>HttpServletRequest</code>
     *                  object that is sent to the servlet
     *
     * @return  a <code>long</code> integer specifying
     *              the time the <code>HttpServletRequest</code>
     *              object was last modified, in milliseconds
     *              since midnight, January 1, 1970 GMT, or
     *              -1 if the time is not known
     */
    protected long getLastModified(HttpServletRequest req) {
        return -1;
    }


    /**
     *  <code>service</code>方法调用此方法处理HEAD类型的http请求。
     *
     * 客户端发送HEAD请求是希望只查看响应头, 例如Content-Type或者Content-Length。
     * HEAD方法计算响应内容的长度来设置Content-Length
     *
     * <p>如果你重写了这个方法，你可以通过避免计算响应内容的长度来提高性能。
     * 不过要记得确保你重写的<code>doHead</code>方法安全且幂等。 (这样多次调用也不会造成问题).
     *
     * qzr-add: 仔细看内容，你会发现，它确实调用的是GET请求来实现的。
     *
     * <p>如果HEAD请求格式错误<code>doHead</code>返回"Bad Request"消息.
     *
     * @param req
     *
     * @param resp
     *
     * @exception IOException  发生输入输出错误
     *
     * @exception ServletException  如果HEAD请求无法被正确处理
     */
    protected void doHead(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        if (DispatcherType.INCLUDE.equals(req.getDispatcherType())) {
            doGet(req, resp);
        } else {
            NoBodyResponse response = new NoBodyResponse(resp);
            doGet(req, response);
            response.setContentLength();
        }
    }


    /**
     * <code>service</code>方法调用此方法处理POST类型的http请求。
     *
     * POST方法允许同一时刻发送无限制长度的数据给服务器并且在发送需要晦涩处理的数据
     * 如信用卡号码的时候，比较优雅.
     *
     * <p>当重写这个方法的时候, 会先读取请求数据, 然后写响应头， 最后编写响应主体。
     * 切记，最好定义好content type和encoding。
     * 当获取<code>PrintWriter</code>来编写响应主体的时候，记住先设置content type.
     *
     * <p>servlet容器返回response消息体之前，必须编写响应头, 因为HTTP头部信息必须先于response消息体.
     *
     * <p>尽量用{@link javax.servlet.ServletResponse#setContentLength}方法指定Content-Length,
     * 因为这样允许servlet会使用一个持久连接来返回数据，提高了性能。
     * 如果整个响应刚好充满一个缓冲区，则会自动设置内容长度。
     *
     * <p>当使用HTTP 1.1 分块编码 这意味着响应具有Transfer-Encoding报头)，不要设置Content-Length报头。
     *
     * <p>这个方法不需要幂等性，他是会改编数据的.
     * POST方法是可以改变状态的，比如修改保存的数据或者在线购物。
     *
     * <p>如果POST请求格式错误<code>doPost</code>返回"Bad Request"消息.
     *
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param resp  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  POST方法无法正确处理的时候
     *
     * @see javax.servlet.ServletOutputStream
     * @see javax.servlet.ServletResponse#setContentType
     */
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        String protocol = req.getProtocol();
        String msg = lStrings.getString("http.method_post_not_supported");
        if (protocol.endsWith("1.1")) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, msg);
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
        }
    }


    /**
     * <code>service</code>方法调用此方法处理PUT类型的http请求。
     *
     * PUT 方法允许客户端放置文件在服务器端，类似FTP的操作。
     *
     * <p>覆盖此方法时，保留随请求一起发送的所有响应头(包括
     * Content-Length, Content-Type, Content-Transfer-Encoding,
     * Content-Encoding, Content-Base, Content-Language, Content-Location,
     * Content-MD5, and Content-Range). 如果你的方法无法处理任意一个头信息，
     * 必须抛出异常(HTTP 501 - Not Implemented)并且抛弃请求。
     * 更多关于HTTP 1.1的信息, 请参考RFC 2616
     * <a href="http://www.ietf.org/rfc/rfc2616.txt"></a>.
     *
     * <p>此方法既不需要保证数据安全也不需要保证幂等性。
     * <code>doPut</code>可以改变数据。
     * 使用此方法时，将受影响的URL的副本保存在临时存储中可能很有用。
     *
     *  <p>如果PUT请求格式错误<code>doPut</code>返回"Bad Request"消息.
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param resp  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  PUT方法无法正确处理的时候
     */
    protected void doPut(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        String protocol = req.getProtocol();
        String msg = lStrings.getString("http.method_put_not_supported");
        if (protocol.endsWith("1.1")) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, msg);
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
        }
    }


    /**
     * <code>service</code>方法调用此方法处理DELETE类型的http请求。
     *
     * DELETE方法允许客户端删除服务器端的文件或者网页。
     *
     * <p>此方法不需要安全和幂等性。使用此方法时，将受影响的URL的副本保存在临时存储中可能很有用。
     *
     *  <p>如果DELETE请求格式错误<code>doDelete</code>返回"Bad Request"消息.
     *
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param resp  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  DELETE方法无法正确处理的时候
     */
    protected void doDelete(HttpServletRequest req,
                            HttpServletResponse resp)
        throws ServletException, IOException {

        String protocol = req.getProtocol();
        String msg = lStrings.getString("http.method_delete_not_supported");
        if (protocol.endsWith("1.1")) {
            resp.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, msg);
        } else {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, msg);
        }
    }


    private static Method[] getAllDeclaredMethods(Class<?> c) {

        if (c.equals(javax.servlet.http.HttpServlet.class)) {
            return null;
        }

        Method[] parentMethods = getAllDeclaredMethods(c.getSuperclass());
        Method[] thisMethods = c.getDeclaredMethods();

        if ((parentMethods != null) && (parentMethods.length > 0)) {
            Method[] allMethods =
                new Method[parentMethods.length + thisMethods.length];
            System.arraycopy(parentMethods, 0, allMethods, 0,
                             parentMethods.length);
            System.arraycopy(thisMethods, 0, allMethods, parentMethods.length,
                             thisMethods.length);

            thisMethods = allMethods;
        }

        return thisMethods;
    }


    /**
     * <code>service</code>方法调用此方法处理OPTIONS类型的http请求。
     *
     * OPTIONS请求确定服务器支持哪些HTTP方法，并返回适当的头。
     * 例如，如果servlet覆盖<code>doGet</code>，则此方法返回以下标头：
     *
     * <p><code>Allow: GET, HEAD, TRACE, OPTIONS</code>
     *
     * <p>没有必要重写此方法，除非servlet实现新的HTTP方法，而不是HTTP 1.1实现的方法。
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param resp  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  OPTIONS方法无法正确处理的时候
     */
    protected void doOptions(HttpServletRequest req,
            HttpServletResponse resp)
        throws ServletException, IOException {

        Method[] methods = getAllDeclaredMethods(this.getClass());

        boolean ALLOW_GET = false;
        boolean ALLOW_HEAD = false;
        boolean ALLOW_POST = false;
        boolean ALLOW_PUT = false;
        boolean ALLOW_DELETE = false;
        boolean ALLOW_TRACE = true;
        boolean ALLOW_OPTIONS = true;

        // Tomcat specific hack to see if TRACE is allowed
        Class<?> clazz = null;
        try {
            clazz = Class.forName("org.apache.catalina.connector.RequestFacade");
            Method getAllowTrace = clazz.getMethod("getAllowTrace", (Class<?>[]) null);
            ALLOW_TRACE = ((Boolean) getAllowTrace.invoke(req, (Object[]) null)).booleanValue();
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException |
                IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            // Ignore. Not running on Tomcat. TRACE is always allowed.
        }
        // End of Tomcat specific hack

        for (int i=0; i<methods.length; i++) {
            Method m = methods[i];

            if (m.getName().equals("doGet")) {
                ALLOW_GET = true;
                ALLOW_HEAD = true;
            }
            if (m.getName().equals("doPost"))
                ALLOW_POST = true;
            if (m.getName().equals("doPut"))
                ALLOW_PUT = true;
            if (m.getName().equals("doDelete"))
                ALLOW_DELETE = true;
        }

        String allow = null;
        if (ALLOW_GET)
            allow=METHOD_GET;
        if (ALLOW_HEAD)
            if (allow==null) allow=METHOD_HEAD;
            else allow += ", " + METHOD_HEAD;
        if (ALLOW_POST)
            if (allow==null) allow=METHOD_POST;
            else allow += ", " + METHOD_POST;
        if (ALLOW_PUT)
            if (allow==null) allow=METHOD_PUT;
            else allow += ", " + METHOD_PUT;
        if (ALLOW_DELETE)
            if (allow==null) allow=METHOD_DELETE;
            else allow += ", " + METHOD_DELETE;
        if (ALLOW_TRACE)
            if (allow==null) allow=METHOD_TRACE;
            else allow += ", " + METHOD_TRACE;
        if (ALLOW_OPTIONS)
            if (allow==null) allow=METHOD_OPTIONS;
            else allow += ", " + METHOD_OPTIONS;

        resp.setHeader("Allow", allow);
    }


    /**
     * <code>service</code>方法调用此方法处理TRACE类型的http请求。
     *
     * TRACE方法将TRACE请求一起发送的头返回给客户机，以便在调试中使用它们。
     * 此方法无需重写。
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param resp  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  TRACE方法无法正确处理的时候
     */
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException
    {

        int responseLength;

        String CRLF = "\r\n";
        StringBuilder buffer = new StringBuilder("TRACE ").append(req.getRequestURI())
            .append(" ").append(req.getProtocol());

        Enumeration<String> reqHeaderEnum = req.getHeaderNames();

        while( reqHeaderEnum.hasMoreElements() ) {
            String headerName = reqHeaderEnum.nextElement();
            buffer.append(CRLF).append(headerName).append(": ")
                .append(req.getHeader(headerName));
        }

        buffer.append(CRLF);

        responseLength = buffer.length();

        resp.setContentType("message/http");
        resp.setContentLength(responseLength);
        ServletOutputStream out = resp.getOutputStream();
        out.print(buffer.toString());
        out.close();
    }


    /**
     * <code>service</code>方法接收HTTP请求并转发给各个对应的<code>do</code><i>Method</i> 方法。
     * 这是servlet的特定方法，无需重写。
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param resp  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  方法无法正确处理的时候
     *
     * @see javax.servlet.Servlet#service
     */
    protected void service(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {

        String method = req.getMethod();

        if (method.equals(METHOD_GET)) {
            long lastModified = getLastModified(req);
            if (lastModified == -1) {
                // servlet不支持if-modified-since, 无需再啰嗦
                doGet(req, resp);
            } else {
                long ifModifiedSince;
                try {
                    ifModifiedSince = req.getDateHeader(HEADER_IFMODSINCE);
                } catch (IllegalArgumentException iae) {
                    // Invalid date header - proceed as if none was set
                    ifModifiedSince = -1;
                }
                if (ifModifiedSince < (lastModified / 1000 * 1000)) { //这么骚的操作是因为等于比较费性能？
                    // If the servlet mod time is later, call doGet()
                    // Round down to the nearest second for a proper compare
                    // A ifModifiedSince of -1 will always be less
                    maybeSetLastModified(resp, lastModified);
                    doGet(req, resp);
                } else {
                    resp.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
                }
            }

        } else if (method.equals(METHOD_HEAD)) {
            long lastModified = getLastModified(req);
            maybeSetLastModified(resp, lastModified);
            doHead(req, resp);

        } else if (method.equals(METHOD_POST)) {
            doPost(req, resp);

        } else if (method.equals(METHOD_PUT)) {
            doPut(req, resp);

        } else if (method.equals(METHOD_DELETE)) {
            doDelete(req, resp);

        } else if (method.equals(METHOD_OPTIONS)) {
            doOptions(req,resp);

        } else if (method.equals(METHOD_TRACE)) {
            doTrace(req,resp);

        } else {
            //
            // Note that this means NO servlet supports whatever
            // method was requested, anywhere on this server.
            //

            String errMsg = lStrings.getString("http.method_not_implemented");
            Object[] errArgs = new Object[1];
            errArgs[0] = method;
            errMsg = MessageFormat.format(errMsg, errArgs);

            resp.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED, errMsg);
        }
    }


    /*
     * Sets the Last-Modified entity header field, if it has not
     * already been set and if the value is meaningful.  Called before
     * doGet, to ensure that headers are set before response data is
     * written.  A subclass might have set this header already, so we
     * check.
     */
    private void maybeSetLastModified(HttpServletResponse resp,
                                      long lastModified) {
        if (resp.containsHeader(HEADER_LASTMOD))
            return;
        if (lastModified >= 0)
            resp.setDateHeader(HEADER_LASTMOD, lastModified);
    }


    /**
     * 分发请求到protected void service方法，无需重写。
     *
     * @param req   {@link HttpServletRequest} 客户端发送给服务器端的信息
     *
     * @param res  {@link HttpServletResponse} 服务器端发送给客户端的信息
     *
     * @exception IOException   输入输出异常
     *
     * @exception ServletException  方法无法正确处理的时候
     *
     * @see javax.servlet.Servlet#service
     *
     * @see javax.servlet.Servlet#service
     */
    @Override
    public void service(ServletRequest req, ServletResponse res)
        throws ServletException, IOException {

        HttpServletRequest  request;
        HttpServletResponse response;

        try {
            request = (HttpServletRequest) req;
            response = (HttpServletResponse) res;
        } catch (ClassCastException e) {
            throw new ServletException(lStrings.getString("http.non_http"));
        }
        service(request, response);
    }
}


/*
 * A response wrapper for use in (dumb) "HEAD" support.
 * This just swallows that body, counting the bytes in order to set
 * the content length appropriately.  All other methods delegate to the
 * wrapped HTTP Servlet Response object.
 */
// file private
class NoBodyResponse extends HttpServletResponseWrapper {
    private final NoBodyOutputStream noBody;
    private PrintWriter writer;
    private boolean didSetContentLength;

    // file private
    NoBodyResponse(HttpServletResponse r) {
        super(r);
        noBody = new NoBodyOutputStream(this);
    }

    // file private
    void setContentLength() {
        if (!didSetContentLength) {
            if (writer != null) {
                writer.flush();
            }
            super.setContentLength(noBody.getContentLength());
        }
    }


    // SERVLET RESPONSE interface methods

    @Override
    public void setContentLength(int len) {
        super.setContentLength(len);
        didSetContentLength = true;
    }

    @Override
    public void setContentLengthLong(long len) {
        super.setContentLengthLong(len);
        didSetContentLength = true;
    }

    @Override
    public void setHeader(String name, String value) {
        super.setHeader(name, value);
        checkHeader(name);
    }

    @Override
    public void addHeader(String name, String value) {
        super.addHeader(name, value);
        checkHeader(name);
    }

    @Override
    public void setIntHeader(String name, int value) {
        super.setIntHeader(name, value);
        checkHeader(name);
    }

    @Override
    public void addIntHeader(String name, int value) {
        super.addIntHeader(name, value);
        checkHeader(name);
    }

    private void checkHeader(String name) {
        if ("content-length".equalsIgnoreCase(name)) {
            didSetContentLength = true;
        }
    }

    @Override
    public ServletOutputStream getOutputStream() throws IOException {
        return noBody;
    }

    @Override
    public PrintWriter getWriter() throws UnsupportedEncodingException {

        if (writer == null) {
            OutputStreamWriter w;

            w = new OutputStreamWriter(noBody, getCharacterEncoding());
            writer = new PrintWriter(w);
        }
        return writer;
    }
}


/*
 * Servlet output stream that gobbles up all its data.
 */

// file private
class NoBodyOutputStream extends ServletOutputStream {

    private static final String LSTRING_FILE =
        "javax.servlet.http.LocalStrings";
    private static final ResourceBundle lStrings =
        ResourceBundle.getBundle(LSTRING_FILE);

    private final HttpServletResponse response;
    private boolean flushed = false;
    private int contentLength = 0;

    // file private
    NoBodyOutputStream(HttpServletResponse response) {
        this.response = response;
    }

    // file private
    int getContentLength() {
        return contentLength;
    }

    @Override
    public void write(int b) throws IOException {
        contentLength++;
        checkCommit();
    }

    @Override
    public void write(byte buf[], int offset, int len) throws IOException {
        if (buf == null) {
            throw new NullPointerException(
                    lStrings.getString("err.io.nullArray"));
        }

        if (offset < 0 || len < 0 || offset+len > buf.length) {
            String msg = lStrings.getString("err.io.indexOutOfBounds");
            Object[] msgArgs = new Object[3];
            msgArgs[0] = Integer.valueOf(offset);
            msgArgs[1] = Integer.valueOf(len);
            msgArgs[2] = Integer.valueOf(buf.length);
            msg = MessageFormat.format(msg, msgArgs);
            throw new IndexOutOfBoundsException(msg);
        }

        contentLength += len;
        checkCommit();
    }

    @Override
    public boolean isReady() {
        // TODO SERVLET 3.1
        return false;
    }

    @Override
    public void setWriteListener(javax.servlet.WriteListener listener) {
        // TODO SERVLET 3.1
    }

    private void checkCommit() throws IOException {
        if (!flushed && contentLength > response.getBufferSize()) {
            response.flushBuffer();
            flushed = true;
        }
    }
}
